
module "lambda_install" {
  source = "./modules/lambda"
  project      = var.project
  component    = var.component
  subcomponent = var.subcomponent
  stage        = var.stage 
  download_url = var.lambda_package_download_url
}

output "lambda_arn" {
  value = module.lambda_install.lambda_arn
}
