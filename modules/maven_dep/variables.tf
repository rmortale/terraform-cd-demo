# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "group_id" {
  description = "groupId of the maven artifact to download"
  type        = string
}

variable "artifact_id" {
  description = "artifactId of the maven artifact to download"
  type        = string
}

variable "artifact_version" {
  description = "version of the maven artifact to download"
  type        = string
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "repo_url" {
  description = "Repository URL of the maven repository"
  type        = string
  default     = "https://repo.maven.apache.org/maven2"
}

variable "packaging" {
  description = "packaging of the maven artifact to download"
  type        = string
  default     = "jar"
}

variable "classifier" {
  description = "classifier of the maven artifact to download"
  type        = string
  default     = ""
}
