data "external" "mvn" {
  program = ["mvn", "org.apache.maven.plugins:maven-dependency-plugin:2.8:get -Dmaven.repo.local=${path.module}/repo -DremoteRepositories=${var.repo_url} -Dartifact=${var.group_id}:${var.artifact_id}:${var.artifact_version}:${var.packaging}:${var.classifier}"]
}

output "external_source" {
  value = data.external.mvn.result
}
