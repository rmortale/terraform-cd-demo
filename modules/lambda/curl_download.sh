#!/bin/bash

set -e

eval "$(jq -r '@sh "DDIR=\(.dir) DURL=\(.download_url)"')"

curl --silent --output $DDIR/function.zip $DURL

jq -n --arg file "$DDIR/function.zip" '{"file":$file}'
