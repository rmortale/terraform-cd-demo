# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "project" {
  description = "The name of the project"
  type        = string
}

variable "component" {
  description = "The name of the component"
  type        = string
}

variable "subcomponent" {
  description = "The name of the subcomponent"
  type        = string
}

variable "stage" {
  description = "The name of the stage"
  type        = string
}

variable "download_url" {
  description = "URL where to download the lambda function code as zip"
  type        = string
}


