locals {
  name = "${var.project}-${var.component}-${var.subcomponent}-${var.stage}"
}

locals {
  package_url = var.download_url
  downloaded  = "downloaded_package_${md5(local.package_url)}.zip"
}

resource "null_resource" "download_package" {
  triggers = {
    downloaded = local.downloaded
  }

  provisioner "local-exec" {
    command = "curl -L -o ${local.downloaded} ${local.package_url}"
  }
}

data "null_data_source" "downloaded_package" {
  inputs = {
    id       = null_resource.download_package.id
    filename = local.downloaded
  }
}

data "aws_iam_policy_document" "policy" {
  statement {
    sid    = ""
    effect = "Allow"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name               =  "${local.name}-role"
  assume_role_policy =  data.aws_iam_policy_document.policy.json
}

resource "aws_iam_role_policy_attachment" "terraform_lambda_policy" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "lambda" {
  function_name = "${local.name}-function"

  filename         = data.null_data_source.downloaded_package.outputs["filename"]
  source_code_hash = filebase64sha256(data.null_data_source.downloaded_package.outputs["filename"])

  role    = aws_iam_role.iam_for_lambda.arn
  handler = "na"
  runtime = "provided"

  environment {
    variables = {
      DISABLE_SIGNAL_HANDLERS = "true"
    }
  }
}

output "lambda_arn" {
  value = aws_lambda_function.lambda.qualified_arn
}
