terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.4.0"
    }
  }
}

provider "aws" {
  region = var.region
  assume_role {
    role_arn = var.workspace_iam_roles[terraform.workspace]
  }
}

terraform {
  backend "s3" {
    bucket   = "terraform-cd-demo-global-state-391684143152"
    key      = "terraform-cd-demo.tfstate"
    encrypt  = true
    region   = "eu-central-1"
    role_arn = "arn:aws:iam::391684143152:role/OrganizationAccountAccessRole"
  }
}

