variable "workspace_iam_roles" {
  default = {
    develop   = "arn:aws:iam::934152566335:role/OrganizationAccountAccessRole"
    stage = "arn:aws:iam::311401440842:role/OrganizationAccountAccessRole"
    prod  = "arn:aws:iam::915661505220:role/OrganizationAccountAccessRole"
  }
}

variable "region" {
  type        = string
  description = "The region in which to create resources."
  default = "eu-central-1"
}

variable "project" {
  type        = string
  description = "Project name"
  default     = "ipoc"
}

variable "component" {
  type        = string
  description = "component name"
  default     = "fileupload"
}

variable "subcomponent" {
  type        = string
  description = "subcomponent name"
  default     = "urlcreator"
}

variable "stage" {
  type        = string
  description = "stage name"
  default     = "develop"
}

variable "lambda_package_download_url" {
  type        = string
  description = "lambda function package download url"
  default     = "https://gitlab.com/rmortale/quarkus-lambda-gitlab/-/package_files/3548015/download"
}